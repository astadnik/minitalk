/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astadnik <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 18:35:10 by astadnik          #+#    #+#             */
/*   Updated: 2020/03/19 15:12:28 by astadnik         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <unistd.h>
# include <stdarg.h>
# include <wchar.h>
# include <limits.h>
# include <time.h>
# include <stdlib.h>
# include <inttypes.h>
# include "../gnl/includes/ft_gnl.h"
# include "../is/includes/ft_is.h"
# include "../lst/includes/ft_lst.h"
# include "../mem/includes/ft_mem.h"
# include "../printf/includes/ft_printf.h"
# include "../put/includes/ft_put.h"
# include "../str/includes/ft_str.h"
# include "../math/includes/ft_math.h"

#endif
